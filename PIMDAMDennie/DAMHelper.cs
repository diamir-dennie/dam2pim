﻿using inRiver.Common.Log;
using inRiver.Remoting.Log;
using inRiver.Remoting.Objects;
using Kahrs.DAM;
using System;
using System.IO;
using static Kahrs.DAM.DAMUtils;

namespace DAM2PIM
{
    public class DAMHelper
    {
        private Logger logger = new Logger();

        public DAMHelper()
        {
        }

        public String ResolveFile(string articleNumber)
        {
            var fileinfo = new FileInfo(articleNumber);


            var url = DAMUtils.GetImageUrl(articleNumber, FilterGroups.Sample, null, null, ((int)DAMMenuIDs.Kahrs).ToString(), Mediaformats.JPG_Medium);

            log(string.Format("Found image for {0} at {1}", articleNumber, url));

            if (string.IsNullOrEmpty(url))
                return null;


            return url;
        }

        public Boolean HasSampleImage(EntityType product)
        {
            return false;
        }

        public void FetchSampleImage(EntityType product)
        {

        }

        public void log(String text)
        {
            LogMessage message = new LogMessage
            {
                EventTime = DateTime.Now,
                Message = text,
                Level = LogLevel.Information
            };
            logger.WriteMessage(message);
        }

    }
}
