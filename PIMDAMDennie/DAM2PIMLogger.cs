﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAM2PIM
{
    using System;
    using System.IO;
    using System.Reflection;
    using inRiver.Common.Log;
    using inRiver.Remoting.Log;

    public class Logger : ILogger
    {

        public LogLevel Level
        {
            get;
            set;
        }

        public string Name
        {
            get { return "DAM2PIM"; }

            set {  }
        }

        public string Settings { get; set; }

        public void WriteMessage(LogMessage message)
        {
            this.Level = LogLevel.Information;

            if (message.Level > this.Level)
            {
                return;
            }

            string assemblypath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string path = string.Format(@"{0}\..\Log\DAM2PIM.log", assemblypath);

            if (!File.Exists(path))
            {
                this.CreateLogFile(path);
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                return;
            }

            using (TextWriter textWriter = TextWriter.Synchronized(File.AppendText(path)))
            {
                textWriter.WriteLine(this.CreateLogMessage(message));

                textWriter.Flush();
                textWriter.Close();
            }
        }

        private string CreateLogMessage(LogMessage message)
        {
            string returnMessage = string.Format("{0} - {1}\t {2}", message.EventTime.ToString("HH:mm:ss"), message.Level, message.Message);

            if (!string.IsNullOrEmpty(message.ExceptionMessage))
            {
                returnMessage += "\r\n\r\n" + message.ExceptionMessage;
            }

            return returnMessage;
        }


        private void CreateLogFile(string logPath)
        {
            if (File.Exists(logPath))
            {
                return;
            }

            using (FileStream fs = File.Create(logPath))
            {
                fs.Close();
            }

            DateTime now = DateTime.Now;

            File.SetCreationTime(logPath, now);
        }
    }
}
