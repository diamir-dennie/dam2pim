﻿using inRiver.Common.Log;
using inRiver.Integration.Configuration;
using inRiver.Integration.Export;
using inRiver.Integration.Interface;
using inRiver.Remoting;
using inRiver.Remoting.Log;
using inRiver.Remoting.Objects;
using System;
using System.IO;
using System.Net;

namespace DAM2PIM
{
    public partial class PIMDAM : ServerListener, IEntityListener
    {

 //       private Logger logger = new Logger();

        public PIMDAM()
        {
            InitializeComponent();

        }

        public override void InitConfigurationSettings()
        {
            ConfigurationManager.Instance.SetConnectorSetting(Id, "ChannelId", "595576");
            ConfigurationManager.Instance.SetConnectorSetting(Id, @"TempDir", "C:\\temp\\D2P");

            base.InitConfigurationSettings();
        }


        public void EntityCommentAdded(int entityId, int commentId)
        {
            throw new NotImplementedException();
        }

        public void EntityCreated(int entityId)
        {
            throw new NotImplementedException();
        }

        public void EntityDeleted(inRiver.Remoting.Objects.Entity deletedEntity)
        {
            throw new NotImplementedException();
        }

        public void EntityFieldSetUpdated(int entityId, string fieldSetId)
        {
            throw new NotImplementedException();
        }

        public void EntityLocked(int entityId)
        {
            throw new NotImplementedException();
        }

        public void EntitySpecificationFieldAdded(int entityId, string fieldName)
        {
            throw new NotImplementedException();
        }

        public void EntitySpecificationFieldUpdated(int entityId, string fieldName)
        {
            throw new NotImplementedException();
        }

        public void EntityUnlocked(int entityId)
        {
            throw new NotImplementedException();
        }

        public void EntityUpdated(int entityId, string[] fields)
        {
            Entity e = RemoteManager.DataService.GetEntity(entityId, LoadLevel.DataAndLinks);

            String url = null;

            string dirPath = @"C:\temp\D2P";
            String productName = null;

            DAMHelper dh = new DAMHelper();

            if (e.EntityType.Id == "Product")
            {

                foreach (Link l in e.OutboundLinks) {

                    if(l.LinkType.ToString() == "ProductItems") { 
                        String ItemNumberDAM = RemoteManager.DataService.GetField(l.Target.Id, "ItemItemNumber").Data.ToString();
                  
                        url = dh.ResolveFile(ItemNumberDAM.Substring(0, 10));
                        productName = ItemNumberDAM.Substring(0, 10);

                        if (!string.IsNullOrEmpty(url))
                            break;
                    }
                }
                
            }

            String fileDir = Path.Combine(dirPath, productName + ".jpg");

            WebClient _wc = new WebClient();
            _wc.DownloadFile(new Uri(url), fileDir);
            _wc.Dispose();
            dh.log(string.Format("Saved temporary image for {0} at {1}", productName, url));

            FileStream fs = new FileStream(fileDir, FileMode.Open);
            byte[] data = new byte[fs.Length];
            fs.Read(data, 0, (int)data.Length);

            int fileId = RemoteManager.UtilityService.AddFile(System.IO.Path.GetFileName(fileDir), data);
            // Get item Resource type
            EntityType resourceEntityType = RemoteManager.ModelService.GetEntityType("Resource");

            Entity resource = Entity.CreateEntity(resourceEntityType);
            resource.Fields.Find(f => f.FieldType.Id == "ResourceName").Data = productName;
            resource.Fields.Find(f => f.FieldType.Id == "ResourceType").Data = "swatchimage";
            resource.Fields.Find(f => f.FieldType.Id == "ResourceFilename").Data = productName + ".jpg";
            resource.Fields.Find(f => f.FieldType.Id == "ResourceFileId").Data = fileId;
            resource.Fields.Find(f => f.FieldType.Id == "ResourceMimeType").Data = "image/jpeg";

            // Add the new item to PIM
            Entity createdResource = RemoteManager.DataService.AddEntity(resource);

/*
            LogMessage message = new LogMessage();
            message.EventTime = DateTime.Now;
            message.Message = string.Format("Resource with id {0} created", createdResource.Id);
            message.Level = LogLevel.Information;
            this.logger.WriteMessage(message);

            message = new LogMessage();
            message.EventTime = DateTime.Now;
            message.Message = string.Format("Resource with id {0} created", createdResource.Id);
            message.Level = LogLevel.Information;
            this.logger.WriteMessage(message);
            //            this.logger.log(string.Format("Resource with id {0} created", createdResource.Id));
*/
            Link productToItemLink = new Link
            {
                LinkType = RemoteManager.ModelService.GetLinkType("ProductResources"),
                Source = e,
                Target = createdResource
            };

            RemoteManager.DataService.AddLinkLast(productToItemLink);

        }
    }
}
